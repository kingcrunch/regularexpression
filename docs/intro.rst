Introduction
============

Notes
-----

Some notable differences to the `PCRE`-extension and other comments.

* `Crunch/RegularExpression` (unlike `PCRE`) distinguishes between the pattern itself and the modifiers. This means, that the
  pattern consumed by this library does not contain the delimiters. It is fixated to `~`, so you need to escape any
  occurrence of `~` like any other character with special meaning to.
* There is a function for it. No need to build everything in OOP. Also the functions helps to create the appropriate
  objects, or return some as the result.
* Because this library is inspired by pythons `re`-module some functions/methods have a different meaning than the
  known. For example `match()` ^always^ matches against the beginning of the string, whereas `find()` is similar
  to the behaviour of `preg_match()`.
* There is a constant for it too. All identifiers, like modifiers, or special character sets, have a corresponding
  constant, like `\\Crunch\\RegularExpression\\Modifiers\\OPTIMIZE` [#]_ to get the modifier `S`. Of course it's not required to
  use them, but sometimes it's useful to make things more readable and obvious.
* This library is (by design) simpler than `PCRE`. This means, that some options are not available, but set to a value,
  that results in "more than needed" in many cases.
* Every time it occurs the definition of `compile()` is probably something different from what you expect: Usually one
  compiles a regular expression to avoid this overhead, when the pattern is used many times, but PHP doesn't support
  to access the the compiled expression directly. However, `PCRE` caches used expressions during per process (up to
  4096). At the end `compile()` just means, that it will return a reusable `Expression`-object with the common pattern
  in it, nothing more, but there is usually no downside in this approach. As a sideeffect: There is no way to purge
  the cache.

Installation
------------
`Crunch/RegularExpression` is not an application, but a library. To integrate it into your application add this to your
`composer.json` and call `php composer.phar update` afterwards.

.. code-block:: yaml

    "require": {
        "crunch/regularexpression": "dev-master@dev"
    }

Because there is no stable release yet, you probably must set `minimum-stability <http://getcomposer.org/doc/04-schema.md#minimum-stability>`_ too

.. code-block:: yaml

    "minimum-stability": "dev"

If your application already uses `composer` it should already contain something like

.. code-block:: php

    require 'path/to/vendor/autoload.php';

If not, add it somewhere within its initialization. Thats all.

For further information see `getcomposer.org <http://getcomposer.org/doc/>`_.

Usage
-----
This library makes use of `Assertions <http://php.net/assert>`_ to find inappropriate use of functions/methods during
development earlier. Remember to `disable evaluation of assertions <php.net/info.configuration.php#ini.assert.active>`_
in production to avoid both unwanted messages and the evaluation overhead.

First remember, that the `API docs are available too <_static/index.html>`_. For a full list of all available
functions, methods, classes and constants, have a look there.

.. code-block:: php

    use Crunch\RegularExpression as re;
    $expression = re\compile($myPattern);

`$expression` is of type `\\Crunch\\RegularExpression\\Expression`. It fact (at least for now) it's just a shortcut for

.. code-block:: php

    $expression = new re\Expression($myPattern);

and it doesn't serve any deeper purpose. It's important to know, that `$myPattern` does _not_ contain the delimiters.
If you want to specifiy special modifiers, pass them as second argument

.. code-block:: php

    $expression = re\compile($myPattern, re\Modifier\CASELESS);
    $expression = new re\Expression($myPattern, re\Modifier\CASELESS);

Except this difference `$myPattern` and the modifiers are exactly the ones expected by the `PCRE`-functions. Now you
can call the different methods on your newly created `Expression`-instance, but for one-time usage there are again
shortcuts for every one of it. This means, everyone of this are doing exactly the same. The main thing to remember is,
that the modifiers appear as the _last_ argument and not directly after the pattern.

.. code-block:: php

    $expr = new re\Exception($myPattern, re\Modifier\CASELESS); $result = $expr->find($subject); // verbose
    $result = (new re\Exception($myPattern, re\Modifier\CASELESS))->find($subject); // Requires PHP5.4
    $result = re\compile($myPattern, re\Modifier\CASELESS)->find($subject);
    $result = re\find($myPattern, $subject, re\Modifier\CASELESS);

I used `re\\Modifier\\CASELESS` as modifier. This is just a constant for the known `i`-modifier used in regular `PCRE`-
patterns. This means, if you want to combine multiple modifiers, you can simply use the string e.g. `"iU"`, or you must
concatenate them like.

.. code-block:: php

    re\Modifier\CASELESS . re\Modifier\UNGREEDY

At all there are two "kinds" of functions/methods: The one, that modify, or extract something from the subject, and
the ones, that just find for a match. The return value of the former ones depends on what they are doing. The latter
ones return either a `Matches`-instance, or an array of `Matches`-instances. This class provides the interface to
the matched result by implementing the `ArrayAccess`-interface. Using `integer` gives the nth matching subpattern
where `0` is the whole matched substring) and using `strings` gives the named subpattern.

.. code-block:: php

    $result = re\find('Foo(?P<myname>Bar)', $string);
    echo $result[0]; // 'FooBar'
    echo $result[1]; // 'bar'
    echo $result['myname'];

Not implemented yet, but planned is to allow to pass `array` to retrieve multiple results at once like

.. code-block:: php

    list($firstname, $lastname, $email) = $result[array('firstname', 'lastname', 'email')];

Note, that with PHP5.4 this leads to a kind of "double-bracket"-syntax and also can be called directly with the
match itself

.. code-block:: php

    list($firstname, $lastname, $email) = re\find($pattern, $subject)[['firstname', 'lastname', 'email']];

If this is useful (means: remains readable) depends on the use-case. It's not implemented yet anyway.

Contribution
------------
The project is located at `github.com <https://github.com/KingCrunch/RegularExpression>`_. Besides code any bug reports,
feature requests, or ideas are appreciated.


Some unordered ideas
--------------------
There is no roadmap yet and there are two reasons for it: First this library should be useful and not as big as possible
just to provide as many features as possible. And the second one is, that I currently just have an eye on pythons
`re`-module and what features it provides. It does not mean, that I'm going to copy it (at least not 1:1, because of
the slightly different nature of both languages), but in my opinion their featureset feels "usefulness-complete" enough
to try to cover the same.

However, there are at least some own ideas I have in mind. Everyone is just an idea and neither the concrete
implementation, nor their usefulness are evaluated yet.

* `Pattern`-class, that allows to build patterns with an OOP-based interface like `$pattern->setModifier(m\\Optimize);`
  and `$pattern->prepend($anotherPattern, $occurrences, $name, $otherFlags);`.
* Some predefined patterns (as string) like `patterns\\URL`.
* Combining existing patterns
* Maybe optimizations of patterns. I can imagine, that this may get quite complex and even further it may don't lead
  to any enhancement.
* Enhanced access of the resulting `Match`-instance depending on the type of the used key. For example
  `$match[array(1,3)]` [#]_ could return the first (`0` is the entire match) and third result.


----

.. [#] Remember, that you can (and should) alias every namespace- and class-identifier:

       .. code-block:: php

            use Crunch\RegularExpression as re;
            use Crunch\RegularExpression\Modifiers as m;
            $expr = re\compile('[a-z0-9]', m\OPTIMIZE . m\CASELESS);

.. [#] PHP5.4 is out: With array dereferencing and short array notation this can be used to created very compacted
       expressions.

       .. code-block:: php

            use Crunch\RegularExpression as re;
            list($name, $email) = re\match('(?P<name>.+) <(?P<email>.+@.+\..{2,5})>', $myString)[['name','email']];

       Not said, that this always a good idea, but maybe sometimes.

       .. code-block::php

            list($name, $email) = re\match(self::AUTHOR, $myString)[['name','email']];
