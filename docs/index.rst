Welcome to Crunch/RegularExpression's documentation!
========================================

`Crunch/RegularExpression` is a wrapper around PHPs `PCRE`-extension to provide a cleaner interface, that simplifies
the handling, especially the creation and reusing of patterns and expression.

`API documentation <_static/index.html>`_ [#]_


Contents:

.. toctree::
   :maxdepth: 2

   intro
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


:Authors:
    Sebastian "KingCrunch" Krebs <krebs.seb@gmail.com>

.. [#] This is taken from https://github.com/weierophinney/phly_mustache/issues/13 Thanks for that example :)
