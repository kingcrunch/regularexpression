<?php
namespace Crunch\RegularExpression;
const DELIMITER = '~';
const VERSION = \PCRE_VERSION;

function escape ($pattern) {
    return \preg_quote($pattern, DELIMITER);
}

/**
 * @param string $pattern
 * @param string|null $modifiers
 * @return \Crunch\RegularExpression\Expression
 */
function compile ($pattern, $modifiers = null) {
    \assert('\is_string($pattern);');
    \assert('\is_string($modifiers) or \is_null($modifiers);');
    return new Expression($pattern, $modifiers);
}

function find ($pattern, $string, $modifiers = null) {
    \assert('\is_string($pattern);');
    \assert('\is_string($string);');
    \assert('\is_int($modifiers) or \is_null($modifiers);');
    return compile($pattern, $modifiers)->find($string);
}

function match ($pattern, $string, $offset = null, $modifiers = null) {
    \assert('\is_string($pattern);');
    \assert('\is_string($string);');
    \assert('\is_int($offset) or \is_null($offset);');
    \assert('\is_string($modifiers) or \is_null($modifiers);');
    return compile($pattern, $modifiers)->match($string, $offset);
}

function split($pattern, $string, $modifiers = null) {
    return compile($pattern, $modifiers)->split($string);
}

function findAll ($pattern, $string, $modifiers = null) {
    \assert('\is_string($pattern);');
    \assert('\is_string($string);');
    \assert('\is_string($modifiers) or \is_null($modifiers);');
    return compile($pattern, $modifiers)->findAll($string);
}

function replace ($pattern, $string, $replacement, $modifiers = null) {
    \assert('\is_string($pattern);');
    \assert('\is_string($string);');
    \assert('\is_string($replacement);');
    \assert('\is_string($modifiers) or \is_null($modifiers);');
    return compile($pattern, $modifiers)->replace($string, $replacement);
}
