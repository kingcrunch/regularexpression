<?php
namespace Crunch\RegularExpression\Modifier;

const CASELESS = 'i';
const MULTILINE = 'm';
const DOTALL = 's';
const EXTENDED = 'x';
const ANCHORED = 'A';
const DOLLAR_ENDONLY = 'D';
const OPTIMIZE = 'S';
const UNGREEDY = 'U';
const EXTRA = 'X';
const UTF8 = 'u';

/**
 * Removes duplicates and sort the modifiers
 *
 * This is a helper function and it's only purpose is to avoid much variation in the resulting pattern, so it maybe
 * result in an already cached one.
 *
 * @param  string $modifiers
 * @return string
 */
function normalize ($modifiers) {
    $modifiers = \array_unique(\str_split($modifiers, 1));
    sort($modifiers);
    return implode('', $modifiers);
}
