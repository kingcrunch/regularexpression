<?php
namespace Crunch\RegularExpression;

/**
 * Object representing the matches, that results from a pattern applied to a subject
 *
 * Immutable: Any access to offsetSet() or offsetSet() will throw an Exception
 *
 * Usually there is no reason to create an instance yourself. It's used as a result of all matching-type
 * methods/functions.
 *
 * As a result of a match it behaves both like a list and a map. Called with an integer argument it returns
 * the value of the n-th-left (sub)pattern, or the full matching string (`0`). Called with a string it returns the
 * value of named pattern (`(?P<name>.+)`) with the given name.
 *
 * - echo $matches[0];
 * - echo $matches[123];
 * - echo $matches['namedPattern'];
 *
 * Additional it's possible to fetch multiple values at once with array as argument
 *
 * - var_dump($matches[array(1,2)]); // Fetches the first two subpatterns
 * - var_dump($matches[array('email', 'name')]); // Fetches the two named subpatterns "email" and "name"
 *
 * Just a little hint
 *
 * - var_dump($matches[range(1,7)]); // Returns the subpatterns 1 to 7
 *
 * The result is now an array.
 */
class Matches implements \ArrayAccess, \Countable {
    /**
     * The named values
     *
     * @var stdClass[string]
     */
    protected $_named = array();

    /**
     * The index values
     *
     * @var stdClass[int]
     */
    protected $_indexed = array();

    /**
     * The expression-object from which this Matches-instance is the result
     *
     * @var Expression
     */
    public $expression;

    /**
     * The subject, that (combined with the expression) leads to this Matches-instance
     *
     * @var string
     */
    public $subject;

    /**
     * Creates new instance
     *
     * The $result-array must be of a special format. It's an array of tupels with the first element the
     * string content, that is covered by the (sub)pattern and the second element is the offset, where the string
     * begins within the subject pattern.
     *
     * @param Expression $expression
     * @param string     $subject
     * @param array      $result
     */
    public function __construct (Expression $expression, $subject, array $result) {
        \assert('\is_string($subject);');
        $this->expression = $expression;
        $this->subject = $subject;
        $lastNumeric = 0;
        foreach (array_keys($result) as $key) {
            if (is_string($key)) {
                $this->_named[$key] = $lastNumeric+1;
            } else {
                $lastNumeric = $key;
                $this->_indexed[$key] = (object) array('value' => $result[$key][0], 'offset' => $result[$key][1], 'name' => null, 'index' => $key);
            }
        }
        foreach ($this->_named as $name => &$id) {
            $this->_indexed[$id]->index = $id;
            $this->_indexed[$id]->name = $name;
            $id = $this->_indexed[$id];
        }
    }

    /**
     * Returns the start-index within the subject of the (sub)pattern by given group-index, or -name
     *
     * @param int|string $group
     * @return int
     */
    public function startOf ($group) {
        $struct = is_string($group) ? $this->_named[$group] : $this->_indexed[$group];
        return $struct->offset;
    }

    /**
     * Returns the end-index within the subject of the (sub)pattern by given group-index, or -name
     *
     * @param int|string $group
     * @return int
     */
    public function endOf ($group) {
        $struct = is_string($group) ? $this->_named[$group] : $this->_indexed[$group];
        return $struct->offset + strlen($struct->value);
    }

    /**
     * Start- and end-index of the group by index or name
     *
     * Returns a 2-tupel (array with two elements) with the first the start- and the second the end-index. In fact
     * it's just
     *
     * - array(startOf($group), endOf($group))
     *
     * @param int|string $group
     * @return array
     */
    public function spanOf ($group) {
        return array($this->startOf($group), $this->endOf($group));
    }

    /**
     * Returns the group-index of the group given by name
     *
     * @param string $name
     * @return int|null
     */
    public function indexOf ($name) {
        return isset($this->_named[$name]) ? $this->_named[$name]->index : null;
    }

    /**
     * Returns the name of the named pattern given by the group-index
     *
     * @param int $index
     * @return string|null
     */
    public function nameOf ($index) {
        return isset($this->_indexed[$index]) ? $this->_indexed[$index]->name : null;
    }

    /**
     * Test, whether a given offset exists
     *
     * Implements ArrayAccess. Returns true, if the one, or all (in case of array) given group-index, or -names exists.
     *
     * @param int|string|array $offset
     * @return bool
     */
    public function offsetExists ($offset) {
        switch (true) {
            case is_array($offset):
                $that = $this;
                return array_reduce($offset, function ($result, $offset) use ($that) { return $result && isset($that[$offset]); }, true);
                break;
            case is_String($offset):
                return isset($this->_named[$offset]);
                break;
            case is_int($offset):
                return isset($this->_indexed[$offset]);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Returns one (or all in case of an array) values associated with the index, or name(s)
     *
     * @param int|string|array  $offset
     * @return string|array
     */
    public function offsetGet ($offset) {
        switch (true) {
            case is_array($offset):
                $that = $this;
                return array_map(function($offset) use ($that) { return $that[$offset]; }, $offset);
                break;
            case !$this->offsetExists($offset):
                return null;
                break;
            case is_string($offset):
                return $this->_named[$offset]->value;
                break;
            case is_int($offset):
                return $this->_indexed[$offset]->value;
                break;
            default:
                return null;
                break;
        }
    }

    /**
     * Unsupported: Immutable
     *
     * @param int|string|array $offset
     * @param string $value
     * @throws \BadMethodCallException
     */
    public function offsetSet ($offset, $value) {
        throw new \BadMethodCallException('Immutable');
    }


    /**
     * Unsupported: Immutable
     *
     * @param int|string|array $offset
     * @throws \BadMethodCallException
     */
    public function offsetUnset ($offset) {
        throw new \BadMethodCallException('Immutable');
    }

    /**
     * Returns the number of matchings
     *
     * Returns always at least 1, because the whole pattern (index `0`) must always have matched, because else the instance
     * wouldn't exists.
     *
     * @return int
     */
    public function count () {
        return \count($this->_indexed);
    }
}
