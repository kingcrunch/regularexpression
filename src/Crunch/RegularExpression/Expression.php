<?php
namespace Crunch\RegularExpression;

use Crunch\RegularExpression\Modifier;
use Crunch\RegularExpression\Pattern\Assertion;

/**
 * The expression, that results from a pattern and "is used"
 *
 */
class Expression {
    /**
     * The pattern to match against _without_ delimiters
     *
     * @var string
     */
    protected $_pattern;

    /**
     * The modifiers to use with this pattern, like "i" (case-insensitive)
     *
     * See `Modifiers`-constants.
     *
     * @var string
     */
    protected $_modifiers;

    /**
     * Creates new `Expression`-instance
     *
     * @param string      $pattern
     * @param string|null $modifiers
     */
    public function __construct ($pattern, $modifiers = null) {
        $this->_pattern = (string) $pattern;
        $this->_modifiers = Modifier\normalize((string) $modifiers);
    }

    /**
     * Searches pattern within the subject
     *
     *
     *
     * @param string $subject
     * @throws \RuntimeException
     * @return Matches|null
     */
    public function find ($subject) {
        $pattern = DELIMITER . $this->_pattern . DELIMITER . $this->_modifiers;
        $matches = array();
        ($result = @\preg_match($pattern, $subject, $matches, \PREG_OFFSET_CAPTURE)) !== false or $this->_error();;
        return $result ? new Matches($this, $subject, $matches) : null;
    }

    /**
     * Matches pattern at the beginning of the string
     *
     * Remind the difference: Unlike `preg_match()` this method only matches against the beginning of the string like
     * if the anchor `^` is prepended. if you are looking for the more `preg_match()`-style method, use `::find()`
     *
     * Returns 'null' on "no match", thus it's directly usable in if()
     *
     * @param string       $subject
     * @param integer|null $offset
     * @return Matches|null
     */
    public function match ($subject, $offset = null) {
        $pattern = DELIMITER .  Assertion\START_OF_MATCH . ltrim($this->_pattern, '^') . DELIMITER . $this->_modifiers;
        $matches = array();
        ($result = @\preg_match($pattern, $subject, $matches, \PREG_OFFSET_CAPTURE, $offset)) !== false or $this->_error();
        return $result ? new Matches($this, $subject, $matches) : null;
    }

    /**
     * Splits the string by the pattern
     *
     * If the pattern contains any (capturing) sub-pattern, it will return the matches as will. This means
     *
     * - The pattern 'Foo' will _not_ contain any occurence of 'Foo', bu
     * - The pattern '(Foo)' _will_ contain it and again
     * - The pattern '(?:Foo)' will _not_ contain it.
     *
     * @param string $subject
     * @return string[]
     */
    public function split ($subject) {
        $pattern = DELIMITER . $this->_pattern . DELIMITER . $this->_modifiers;
        ($result = @preg_split($pattern, $subject, null, \PREG_SPLIT_NO_EMPTY | \PREG_SPLIT_DELIM_CAPTURE)) !== false or $this->_error();

        return $result;
    }

    /**
     * Like `find()`, but returns an array of every match against the subject
     *
     * @param string $subject
     * @return Matches[]
     */
    public function findAll ($subject) {
        $pattern = DELIMITER . $this->_pattern . DELIMITER . $this->_modifiers;
        $matches = array();
        ($result = @\preg_match_all($pattern, $subject, $matches, \PREG_SET_ORDER | \PREG_OFFSET_CAPTURE)) !== false or $this->_error();
        $that = $this; // 5.3 compatibility
        return \array_map(function (array $match) use ($subject, $that) { return new Matches($that, $subject, $match); },$matches);
    }

    /**
     * Replace every occurrence of pattern with the replacement
     *
     * The replacement may either be a string, where it is used as template (means: It includes back references like
     * \\1), or it may be an callback. In this case the callback is called with every matched `Matches`-instance
     * and it must return a string.
     *
     * @param string $subject
     * @param callable(Matches)|string $replacement
     * @return mixed
     */
    public function replace ($subject, $replacement) {
        $pattern = DELIMITER . $this->_pattern . DELIMITER . $this->_modifiers;
        if (is_callable($replacement)) {
            $that = $this; // 5.3 compatibility
            $replacement = function ($match) use ($replacement, $subject, $that) { return $replacement(new Matches($that, $subject, $match)); };
            ($result = @\preg_replace_callback($pattern, $replacement, $subject, null)) !== false or $this->_error();
        } else {
            ($result = @\preg_replace($pattern, $replacement, $subject, null)) !== false or $this->_error();
        }
        return $result;
    }

    /**
     * Returns the (full) pattern as string
     *
     * @return string
     */
    public function __toString () {
        return DELIMITER . $this->_pattern . DELIMITER . $this->_modifiers;
    }

    /**
     * Compares to expressions by comparing their string representation
     *
     * @param Expression $that
     * @return bool
     */
    public function isEqualTo (self $that) {
        return (string) $this == (string) $that;
    }

    /**
     * Helperfunction, that throws an appropriate Exception, if recently an error occurs
     *
     * It should only get called, when there is really an error before, because else it may cover a much older error,
     * that is completely independent from the current context.
     *
     * @throws \RuntimeException
     */
    protected function _error() {
        $error = preg_last_error();
        if ($error == PREG_NO_ERROR) {
            $error = error_get_last();
            throw new \RuntimeException($error['message'], $error['type']);
        } else {
            throw new \RuntimeException('PCRE ' . $error);
        }
    }
}
