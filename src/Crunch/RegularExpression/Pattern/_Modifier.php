<?php
namespace Crunch\RegularExpression\Pattern\Modifier;

const CASELESS = 'i';
const MULTILINE = 'm';
const DOTALL = 's';
const EXTENDED = 'x';
const UNGREEDY = 'U';
const EXTRA = 'X';
