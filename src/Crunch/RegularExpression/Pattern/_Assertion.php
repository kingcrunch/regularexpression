<?php
namespace Crunch\RegularExpression\Pattern\Assertion;

const WORD_BOUNDARY = '\b';
const NOT_WORD_BOUNDARY = '\B';
const START_OF_SUBJECT = '\A';
const END_OF_SUBJECT = '\z';
const END_OF_SUBJECT_OR_NEWLINE = '\Z';
const START_OF_MATCH = '\G';
