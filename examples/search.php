<?php
namespace Crunch\Example;

use Crunch\RegularExpression\Expression;
use Crunch\RegularExpression as re;

require __DIR__ . '/../vendor/autoload.php'; // Init

$expression = new Expression('Foo(?P<hasAName>Bar)?');
var_dump($expression->find('Fookdasda'));
var_dump($expression->find('dasdaFooBar'));
var_dump($expression->find('SomethingDifferent'));

// compile() is a factory for Expression
var_dump(re\compile('Foo(?P<hasAName>Bar)?')->find('dasdaFooBar'));

// Shortcut of compile()->find()
var_dump(re\search('Foo(?P<hasAName>Bar)?', 'dasdaFooBar'));

