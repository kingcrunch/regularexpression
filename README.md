Crunch\RegularExpression [![Build Status](https://secure.travis-ci.org/KingCrunch/RegularExpression.png)](http://travis-ci.org/KingCrunch/RegularExpression)
============
Regular expression library

* [Documentation at readthedocs.org](http://crunchregularexpression.readthedocs.org/en/latest/) (Including API-Doc)
* [List of available packages at packagist.org](http://packagist.org/packages/crunch/regularexpression)
  (See also [Composer: Declaring dependencies](http://getcomposer.org/doc/00-intro.md#declaring-dependencies))

Requirements
============
* PHP => 5.3

Contributors
============
See CONTRIBUTING.md for details on how to contribute.

* Sebastian "KingCrunch" Krebs <krebs.seb@gmail.com> -- http://www.kingcrunch.de/ (german)

License
=======
This library is licensed under the MIT License. See the LICENSE file for details.
