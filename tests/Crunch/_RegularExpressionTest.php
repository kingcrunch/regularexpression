<?php
namespace Crunch\RegularExpression;

use Crunch\RegularExpression\Modifier as m;

class _RegexTest extends \PHPUnit_Framework_TestCase {
    public function testCompile () {
        $expr = compile('Foo');
        $this->assertEquals('~Foo~', (string) $expr);
    }
    public function testCompileWithModifier () {
        $expr = compile('Foo', m\CASELESS);
        $this->assertEquals('~Foo~i', (string) $expr);
    }
    public function testExpressionDoesNotFindAnything () {
        $this->assertNull(find('Foo', 'Bar'));
    }
    public function testExpressionDoesFindSomething () {
        $this->assertNotNull(find('Foo', 'BarFoo'));
    }

    public function testMatchDoesntMatch () {
        $this->assertNull(match('Foo', 'BarFoo'));
    }

    public function testMatchMatches () {
        $this->assertNotNull(match('Foo', 'FooBar'));
    }

    public function testMatchDoesntMatchWithOffset () {
        $this->assertNull(match('Foo', 'FooBarFoo', 3));
    }

    public function testMatchMatchesWithOffset () {
        $this->assertNotNull(match('Foo', 'BarFoo', 3));
    }
}
