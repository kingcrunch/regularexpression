<?php
namespace Crunch\RegularExpression;

class ExpressionTest extends \PHPUnit_Framework_TestCase {
    public function testExpressionDoesNotFindAnything () {
        $expr = new Expression('Foo');
        $this->assertNull($expr->find('Bar'));
    }
    public function testExpressionDoesFindSomething () {
        $expr = new Expression('Foo');
        $this->assertNotNull($expr->find('BarFoo'));
    }

    public function testMatchDoesntMatch () {
        $expr = new Expression('Foo');
        $this->assertNull($expr->match('BarFoo'));
    }

    public function testMatchMatches () {
        $expr = new Expression('Foo');
        $this->assertNotNull($expr->match('FooBar'));
    }

    public function testMatchDoesntMatchWithOffset () {
        $expr = new Expression('Foo');
        $this->assertNull($expr->match('FooBarFoo', 3));
    }

    public function testMatchMatchesWithOffset () {
        $expr = new Expression('Foo');
        $this->assertNotNull($expr->match('BarFoo', 3));
    }
}
